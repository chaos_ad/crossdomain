-module(crossdomain_connection).
-behaviour(ranch_protocol).

-export([start_link/4]).
-export([init/4]).

start_link(Ref, Socket, Transport, Opts) ->
    Pid = spawn_link(?MODULE, init, [Ref, Socket, Transport, Opts]),
    {ok, Pid}.

init(Ref, Socket, Transport, _Opts = []) ->
    ok = ranch:accept_ack(Ref),
    {ok, {IP, Port}} = Transport:peername(Socket),
    Endpoint = lists:flatten(io_lib:format("~s:~B", [inet_parse:ntoa(IP), Port])),
    loop(Socket, Transport, Endpoint).

loop(Socket, Transport, Endpoint) ->
    lager:debug("['~s']: accepted", [Endpoint]),
    case Transport:recv(Socket, 23, 5000) of
        {ok, <<"<policy-file-request/>", 0:8>>} ->
            lager:debug("['~s']: policy request received", [Endpoint]),
            Transport:send(Socket, policy_file());
        {ok, Invalid} ->
            lager:debug("['~s']: invalid request received: ~p", [Endpoint, Invalid]);
        {error, timeout} ->
            lager:debug("['~s']: timeout", [Endpoint]);
        {error, _} ->
            lager:debug("['~s']: closed", [Endpoint]);
        Other ->
            lager:debug("['~s']: ~p", [Endpoint, Other])
    end,
    ok = Transport:close(Socket).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

policy_file() ->
    <<"<?xml version=\"1.0\"?>"
      "<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">"
      "<cross-domain-policy>"
      "<site-control permitted-cross-domain-policies=\"master-only\"/>"
      "<allow-access-from domain=\"*\" to-ports=\"*\" />"
      "</cross-domain-policy>\"">>.
